#Models
from myapp.models import Companies, CompanyUsers,WeeklyEmailSummary

#helpers
from datetime import datetime , timedelta
from myapp.scheduler.analytics import google_accounts, sessions, leads, channels, sources, queries,weeklyEmailSummary


def collectReports():
    start_date = datetime.date(datetime.now())-timedelta(days=30)
    end_date = datetime.date(datetime.now())-timedelta(days=1)
    print('Starting script at:'+str(datetime.date(datetime.now())))
    list_accounts = Companies.objects.all()
    for company in list_accounts:
        collectCompanyData(company, start_date, end_date)
    print('Ending script at:'+str(datetime.date(datetime.now())))
    
def collectCompanyData(company, start_date, end_date):
    # No Creds, No Fun
    if company.credentials is None or company.credentials is '':
        return
    profileId=google_accounts(company,0)
    # No website, No Further
    if 'id' not in profileId:
        return
    profileId=profileId.get('id')
    if company.last_fetched is not "" and  company.last_fetched is not None:
        start_date=company.last_fetched
        
    #start gathering reports
    print('Fetching data for '+company.company_name+' from '+str(start_date)+' to '+str(end_date))
    try:
        sessions(company, profileId, start_date, end_date)
        leads(company, profileId, start_date, end_date)
        channels(company, profileId, start_date, end_date)
        sources(company, profileId, start_date, end_date)
        queries(company, start_date, end_date)
        Companies.objects.filter(id = company.id).update(last_fetched = end_date)
        
    except Exception as e:
        print(str(e))

def weeklyEmail():
    print('Sending Notification Email')
    # ~ companyusers = CompanyUsers.objects.all()
    weeklynotifications = WeeklyEmailSummary.objects.filter(notify_weekly = True).all()
    for weeklyNotification in weeklynotifications:
        try:
            notifyWeekly = weeklyEmailSummary(weeklyNotification)
        except Exception as e:
            print(str(e)) 
    print('sent')
