#Models
from django.contrib.auth.models import User
from myapp.models import Profile , Analytics ,Siteurls, Providers,Companies,CompanyUsers,AnalyticsSessions,AnalyticsLeads,AnalyticsChannels,AnalyticsSources,AnalyticsQuerries,WeeklyEmailSummary,HiddenLeads,AnalyticsQuerries,AnalyticsPages,AnalyticsCountries,AnalyticsDevices
#google libraries
import google.oauth2.credentials
import google_auth_oauthlib.flow
from apiclient.discovery import build
#helpers
import json
from django.template.loader import render_to_string
from datetime import datetime , timedelta
from django.conf import settings as configuration
import sendgrid
from sendgrid.helpers.mail import *

# function return profile id of the website under company
def google_accounts(company,websiteIndex=0):
    clientId = company.id
    sessionCredentials =json.loads(company.credentials) 
    credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
    drive = build('analytics', 'v3', credentials=credentials)   
    list_accounts = company
    #TODO: save profile ID in DB and return directly from db instead of fetching from google again
    if list_accounts.accounts_data == "" and list_accounts.console_url == "" and list_accounts.profile_id is None:
        accounts = drive.management().accounts().list().execute()
        data = json.dumps(accounts)
        consoleservice = build('webmasters', 'v3', credentials=credentials)
        site_list = consoleservice.sites().list().execute()
        sites = json.dumps(site_list)
        urls =  Companies.objects.filter(id = clientId).update(accounts_data = data , console_url = sites)
        
        data1 = json.loads(data)
        site_urls = sites
        sites = json.loads(site_urls)
        
        if 'items' not in data1:
            return {}
        
        account = data1['items'][websiteIndex].get('id')
        properties = drive.management().webproperties().list(accountId=account).execute()
        
        if 'items' not in properties:
            return {}
        
        website_url = properties.get('items')[0].get('websiteUrl')
        if list_accounts.analytics_url == "" or list_accounts.analytics_url != website_url:
            Companies.objects.filter(id = clientId).update(analytics_url = website_url)
        property = properties.get('items')[0].get('id')
        profiles = drive.management().profiles().list(accountId=account,webPropertyId=property).execute()
        
        if 'items' not in profiles:
            return {}
        
        # return the first view (profile) id.
        profileId = profiles.get('items')[0].get('id')
        Companies.objects.filter(id = clientId).update(profile_id = profileId)
        return { 'id': profileId}
    else:
        profileId = list_accounts.profile_id
        return { 'id': profileId}

# function fetches the sessions of the website
def sessions(company, profile_id, start_date, end_date):
    client_id = company.id
    sessionCredentials =json.loads(company.credentials) 
    clientId = client_id
    credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
    analytics = build('analytics', 'v4', credentials=credentials, discoveryServiceUrl = configuration.WHITELABEL_DISCOVERY_URI)
    response = analytics.reports().batchGet(
          body={
            'reportRequests': [
            {
              'viewId': profile_id,
              'dateRanges': [{'startDate': str(start_date), 'endDate': str(end_date)}],
              'metrics': [{'expression': 'ga:sessions'},{'expression': 'ga:newUsers'},{'expression': 'ga:bounceRate'}],
              'dimensions':[{ 'name': 'ga:date' }],
            }]
          }
      ).execute()
    metrics_data={}
    if len(response.get('reports')) > 0:
        rep = response.get('reports')[0]
        # ~ for report in response.get('reports', []):
        metricHeaders = rep.get('columnHeader').get('metricHeader').get('metricHeaderEntries')
        dimensionHeaders = rep.get('columnHeader').get('dimensions')
        metrics_values=[]
        session_analytics=[]
        if rep.get('data').get('rowCount') is None:
            print('No session data for '+company.company_name)
            return
        if rep.get('data').get('rowCount') > 0:
            rows = rep.get('data').get('rows', [])
            for row in rows:
                row_temp = []
                dimension_values = row.get('dimensions', [])
                metrics = row.get('metrics', [])
                for d in dimension_values:
                    row_temp.append(d)
                for m in metrics[0]['values']:
                    row_temp.append(m)
                leadDate = datetime.strptime(row_temp[0], "%Y%m%d").date();
                session_analytics.append(AnalyticsSessions(company_id = company.id,bounce_rate = row_temp[3], traffic = row_temp[1], new_users = row_temp[2],lead_date = leadDate))
            AnalyticsSessions.objects.bulk_create(session_analytics)
        print('Got session data for '+company.company_name)
    else:
        print('No session data for '+company.company_name)
def leads(company, profile_id, start_date, end_date):
    client_id = company.id
    sessionCredentials =json.loads(company.credentials) 
    clientId = client_id
    credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
    analytics = build('analytics', 'v4', credentials=credentials, discoveryServiceUrl = configuration.WHITELABEL_DISCOVERY_URI)
    
    exceptThese = Providers.objects.all().values_list('provider_name', flat=True)
    exceptThese = list(exceptThese)
    
    response = analytics.reports().batchGet(
          body={
            'reportRequests': [
            {
              'viewId': profile_id,
              'dateRanges': [{'startDate': str(start_date), 'endDate': str(end_date)}],
              'metrics': [{'expression': 'ga:sessionDuration'},{'expression': 'ga:bounces'}],
              'dimensions':[{ 'name': 'ga:networkLocation' },{ 'name': 'ga:source' },{ 'name': 'ga:country' },{ 'name': 'ga:date' }],
              "dimensionFilterClauses": [
                { "operator": "AND",
                  "filters": [
                    {
                      "dimensionName": "ga:networkLocation",
                      "not": True,
                      "operator": "IN_LIST",
                      "expressions": [exceptThese]
                    },
                    {
                      "dimensionName": "ga:networkLocation",
                      "not": True,
                      "operator": "BEGINS_WITH",
                      "expressions": [exceptThese]
                    }
                  ]
                }
              ],
            }]
          }
      ).execute()
    
    dimensions_data={}
    if len(response.get('reports')) > 0:
        header_row = []
        rep = response.get('reports')[0]
        metricHeaders = rep.get('columnHeader').get('metricHeader').get('metricHeaderEntries')
        dimensionHeaders = rep.get('columnHeader').get('dimensions')
        # ~ pagination = rep.get('nextPageToken')
        # ~ print(pagination)
        for dheader in dimensionHeaders:
            header_row.append(dheader.split(":")[1])
        dimensions_values=[]
        dimensions = []
        tempDimension = []
        analyticsLeads=[]
        # ~ print(rep.get('data').get('rowCount'))
        if rep.get('data').get('rowCount') is None:
            print('No leads data for '+company.company_name)
            return
        if rep.get('data').get('rowCount') > 0:
            rows = rep.get('data').get('rows', [])
            for row in rows:
                row_temp = []
                dimension_values = row.get('dimensions', [])
                if dimension_values[0] in tempDimension:
                    tmpCount = tempDimension.index(dimension_values[0])
                else:
                    tempDimension.append(dimension_values[0])
                    metrics = row.get('metrics', [])
                    for d in dimension_values:
                        row_temp.append(d)
                    leadDate = datetime.strptime(row_temp[3], "%Y%m%d").date();
                    # ~ print(temp.strftime('%d/%m/%y'))
                    analyticsLeads.append(AnalyticsLeads(company_id = company.id,lead_name = row_temp[0], source = row_temp[1], country = row_temp[2],lead_date = leadDate))
        # save all leads
        AnalyticsLeads.objects.bulk_create(analyticsLeads)
        print('Got leads data for'+company.company_name)
    else:
        print('No leads data for '+company.company_name)

def channels(company, profile_id, start_date, end_date):
    client_id = company.id
    sessionCredentials =json.loads(company.credentials) 
    clientId = client_id
    credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
    analytics = build('analytics', 'v4', credentials=credentials, discoveryServiceUrl = configuration.WHITELABEL_DISCOVERY_URI)
    
    response = analytics.reports().batchGet(
          body={
            'reportRequests': [
            {
              'viewId': profile_id,
              'dateRanges': [{'startDate': str(start_date), 'endDate': str(end_date)}],
              'metrics': [{'expression': 'ga:users'},{'expression': 'ga:sessions'}],
              'dimensions':[{ 'name': 'ga:channelGrouping' },{ 'name': 'ga:date' }],
            }]
          }
      ).execute()
    data = []
    dimensions_data={}
    if len(response.get('reports')) > 0:
        header_row = []
        rep = response.get('reports')[0]
        metricHeaders = rep.get('columnHeader').get('metricHeader').get('metricHeaderEntries')
        dimensionHeaders = rep.get('columnHeader').get('dimensions')
        if rep.get('data').get('rowCount') is None:
            print('No channel data for '+company.company_name)
            return
        if rep.get('data').get('rowCount') > 0:
            channelLeads=[]
            rows = rep.get('data').get('rows', [])
            for row in rows:
                row_temp = []
                dimension_values = row.get('dimensions', [])
                metrics = row.get('metrics', [])
                for d in dimension_values:
                    row_temp.append(d)
                for m in metrics[0]['values']:
                    row_temp.append(m)
                print(row_temp)
                leadDate = datetime.strptime(row_temp[1], "%Y%m%d").date();
                channelLeads.append(AnalyticsChannels(company_id = company.id,channels = row_temp[0],lead_date = leadDate, users = row_temp[2], sessions = row_temp[3]))
                
             # save all channels
            AnalyticsChannels.objects.bulk_create(channelLeads)
        print('Got channels data for'+company.company_name)
    else:
        print('No channels data for '+company.company_name)
def sources(company, profile_id, start_date, end_date):
    client_id = company.id
    sessionCredentials =json.loads(company.credentials) 
    clientId = client_id
    credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
    analytics = build('analytics', 'v4', credentials=credentials, discoveryServiceUrl = configuration.WHITELABEL_DISCOVERY_URI)
    
    response = analytics.reports().batchGet(
              body={
                'reportRequests': [
                {
                  'viewId': profile_id,
                  'dateRanges': [{'startDate': str(start_date), 'endDate': str(end_date)}],
                  'metrics': [{'expression': 'ga:users'},{'expression': 'ga:sessions'}],
                  'dimensions':[{ 'name': 'ga:source' },{ 'name': 'ga:date' }],
                }]
              }
        ).execute()
    data = []
    dimensions_data={}
    if len(response.get('reports')) > 0:
        header_row = []
        rep = response.get('reports')[0]
        metricHeaders = rep.get('columnHeader').get('metricHeader').get('metricHeaderEntries')
        dimensionHeaders = rep.get('columnHeader').get('dimensions')
        if rep.get('data').get('rowCount') is None:
            print('No source data for '+company.company_name)
            return
        if rep.get('data').get('rowCount') > 0:
            rows = rep.get('data').get('rows', [])
            temp = 1;
            sourceLeads=[]
            for row in rows:
                row_temp = []
                dimension_values = row.get('dimensions', [])
                metrics = row.get('metrics', [])
                for d in dimension_values:
                    row_temp.append(d)
                for m in metrics[0]['values']:
                    row_temp.append(m)
                leadDate = datetime.strptime(row_temp[1], "%Y%m%d").date();
                sourceLeads.append(AnalyticsSources(company_id = company.id,sources = row_temp[0],lead_date = leadDate, users = row_temp[2], sessions = row_temp[3]))
            AnalyticsSources.objects.bulk_create(sourceLeads)
        print('Got Sources data for'+company.company_name)
    else:
        print('No sources data for '+company.company_name)
def queries(company, start_date, end_date):
    client_id = company.id
    company = Companies.objects.get(id = client_id)
    sessionCredentials =json.loads(company.credentials)
    clientId = client_id
    credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
    consoleservice = build('webmasters', 'v3', credentials=credentials)
    sites = json.loads(company.console_url)
    verified_sites_urls = [s['siteUrl'] for s in sites['siteEntry']
                    if s['permissionLevel'] != 'siteUnverifiedUser'
                        and s['siteUrl'][:4] == 'http']
    url = verified_sites_urls
    response = consoleservice.searchanalytics().query(siteUrl=url[0], body={'startDate': str(start_date),'endDate': str(end_date),'dimensions': ['query','date']}).execute()
    result = consoleservice.searchanalytics().query(siteUrl=url[0], body={'startDate': str(start_date),'endDate': str(end_date),'dimensions': ['page','date']}).execute()
    result_country = consoleservice.searchanalytics().query(siteUrl=url[0], body={'startDate': str(start_date),'endDate': str(end_date),'dimensions': ['country','date']}).execute()
    result_device = consoleservice.searchanalytics().query(siteUrl=url[0], body={'startDate': str(start_date),'endDate': str(end_date),'dimensions': ['device','date']}).execute()
    #for queries
    if 'rows' not in response:
        print('No queries for '+company.company_name)
    else:
        querries = response['rows']
        querryConsole = []
        for query in querries:
            querryConsole.append(AnalyticsQuerries(company_id = company.id,impressions = query['impressions'],clicks = query['clicks'], ctr = query['ctr'], position = query['position'],querries = query['keys'][0],lead_date = query['keys'][1]))
        AnalyticsQuerries.objects.bulk_create(querryConsole)
        print('Maybe got queries data for '+company.company_name)
    
    #for pages
    if 'rows' not in result:
        print('No pages  for '+company.company_name)
    else:
        pages = result['rows']
        pageConsole = []
        for page in pages:
            pageConsole.append(AnalyticsPages(company_id = company.id,impressions = page['impressions'],clicks = page['clicks'], ctr = page['ctr'], position = page['position'],pages = page['keys'][0],lead_date = page['keys'][1]))
        AnalyticsPages.objects.bulk_create(pageConsole)
        print('Maybe got pages data for '+company.company_name)
        
    #for countries
    if 'rows' not in result_country:
        print('No countries for '+company.company_name)
    else:
        countries = result_country['rows']
        countryConsole = []
        for country in countries:
            countryConsole.append(AnalyticsCountries(company_id = company.id,impressions = country['impressions'],clicks = country['clicks'], ctr = country['ctr'], position = country['position'],countries = country['keys'][0],lead_date = country['keys'][1]))
        AnalyticsCountries.objects.bulk_create(countryConsole)
        print('Maybe got countries data for '+company.company_name)
    
    #for devices
    if 'rows' not in result_device:
        print('No devices for '+company.company_name)
    else:
        devices = result_device['rows']
        deviceConsole = []
        for device in devices:
            deviceConsole.append(AnalyticsDevices(company_id = company.id,impressions = device['impressions'],clicks = device['clicks'], ctr = device['ctr'], position = device['position'],devices = device['keys'][0],lead_date = device['keys'][1]))
        AnalyticsDevices.objects.bulk_create(deviceConsole)
        print('Maybe got devices data for '+company.company_name)
    
def weeklyEmailSummary(weeklyNotification):
    email = weeklyNotification.company_user.user.email
    
    subject = "Notification Email"
    message = render_to_string('email_templates/weekly_email.html', {
            'weeklyNotification': weeklyNotification
            })
    user.email_user(subject, message)
    print("Notification Email sent successfully")
    # ~ try:
        # ~ emailSummary = WeeklyEmailSummary.objects.get(company_user_id = companyuser.id)
        # ~ if emailSummary.notify_weekly == True:
            # ~ sg = sendgrid.SendGridAPIClient(apikey=configuration.SENDGRID_API_KEY)
            # ~ from_email = Email("noreply@whitelabel.com")
            # ~ to_email = Email(email)
            # ~ subject = "Notification Email"
            # ~ message = render_to_string('email_templates/weekly_email.html', {
                    # ~ 'user': company
                    # ~ })
            # ~ content = Content("text/plain", message)
            # ~ data ={ "to" : [to_email],"subject": subject, "from": from_email ,"content":[ content] }
            # ~ mail = Mail(from_email, subject, to_email, content)
            # ~ response = sg.client.mail.send.post(request_body=mail.get())
            # ~ print("Notification Email sent successfully")
        # ~ else:
            # ~ print("User unsubscribe the weekly notifications")
    # ~ except WeeklyEmailSummary.DoesNotExist:
        # ~ print("No any website notification presents.") 
    # ~ except Exception as e:
        # ~ print(str(e)) 
    
