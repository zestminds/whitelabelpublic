from django.contrib.auth import login as dj_login , authenticate ,logout as signout, update_session_auth_hash
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm ,PasswordResetForm ,PasswordChangeForm
from .forms import CustomUserCreationForm , profileForm,CustomInviteUserForm
from myapp.tokens import account_activation_token
from django.shortcuts import render, redirect
from django.conf import settings as configuration
from django.contrib.auth.models import User
#~ from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import send_mail , EmailMultiAlternatives
from django.template.loader import get_template, render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.contrib import messages
from myapp.models import Profile , Analytics ,Siteurls, Providers,Companies,CompanyUsers,AnalyticsSessions,AnalyticsLeads,AnalyticsChannels,AnalyticsSources,AnalyticsQuerries,WeeklyEmailSummary,HiddenLeads,AnalyticsPages,AnalyticsCountries,AnalyticsDevices
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from django.http import JsonResponse, HttpResponse
import django
import pandas
import random
import string
import requests
from django.core.exceptions import ValidationError
# For making http requests
#import requests
# Google library
import google.oauth2.credentials
import google_auth_oauthlib.flow
from datetime import datetime , timedelta
from apiclient.discovery import build
import sendgrid
from sendgrid.helpers.mail import *
from django.core import serializers
from django.db.models import Prefetch, Q ,Avg , Sum
# from oauth2client.contrib.django_orm import Storage

#for get analytics data and profile id
from myapp.scheduler.main import collectCompanyData, weeklyEmail

# create a function to resolve email to username
def get_user(email):
    try:
        user = User.objects.get(email=email.lower())
        return user.username
    except User.DoesNotExist:
        return None
    
def login(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        password = request.POST.get('password')
        username = get_user(email)
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active and user.is_superuser == False:
                # ~ print(request.session['credentials'])
                dj_login(request, user)
                current_user = user.id 
                
                return redirect('dashboard')
            elif user.is_active and user.is_superuser == True:
                dj_login(request, user)
                current_user = user.id 
                return redirect('/admin')
            else:
                messages.error(request,'Your Account is inactive.')
                return redirect('/login')        
        else:
            messages.error(request,'Email or Password is incorrect')
            return redirect('/login')
    elif request.user.is_authenticated == True:
        return redirect('/')
    else:
        return render(request, 'registration/login.html')
        
def accounts_credentials(request,client_id):
    if client_id == None:
        if request.user.is_authenticated == True:
            user = Profile.objects.get(user_id = request.user.id)
            # ~ print(user.id)
            # ~ company_users = CompanyUsers.objects.filter(user_id = user.id).select_related('company').all()
            if user.agency_id == None and user.is_agency == True:
                credentials = Companies.objects.filter(agency_id = request.session['_auth_user_id']).all()
                if not credentials:
                    session_credentials = None
                    get_client_id = None
                else:
                    if credentials[0].credentials == "":
                        session_credentials = None
                        get_client_id = credentials[0].id
                    else:
                        session_credentials = json.loads(credentials[0].credentials)
                        get_client_id = credentials[0].id
            else:
                credentials = CompanyUsers.objects.filter(user_id = request.session['_auth_user_id']).all()
            # ~ print(company_user[0].company.id)
                # ~ credentials = Companies.objects.filter(agency_id = user.agency_id).all()
                if credentials[0].company.credentials == "":
                    session_credentials = None
                    get_client_id = credentials[0].company.id
                else:
                    session_credentials = json.loads(credentials[0].company.credentials)
                    get_client_id = credentials[0].company.id
    else:
        googleCredentials = Companies.objects.filter(id = client_id).get()
        if googleCredentials.credentials == "":
            session_credentials = None
            get_client_id = googleCredentials.id
        else:
            session_credentials = json.loads(googleCredentials.credentials)
            get_client_id = googleCredentials.id
    return {"session_credentials":session_credentials , "get_client_id":get_client_id}
    
            

def dashboard(request):
    client_id = request.GET.get('client_id')
    if request.user.is_authenticated == True and request.user.is_superuser == False:
        sessionCredentials = accounts_credentials(request,client_id).get("session_credentials")
        clientId = accounts_credentials(request,client_id).get("get_client_id")
        if sessionCredentials == None and clientId is None:
            companyName = None
            credentials = None

        elif sessionCredentials == None:
            credentials = None
            companyName = Companies.objects.filter(id = clientId).get()
        else:
            companyName = Companies.objects.filter(id = clientId).get()
            credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
        return render(request,'dashboard.html', { 'auth_credentials': credentials,"client_id" : clientId,"companyName":companyName })
    elif request.user.is_authenticated == True and request.user.is_superuser == True:
        messages.error(request,'You are loggedIn with admin user. Dashboard is accessible only for normal users')
        return redirect('/admin')
    else:
        credentials = None
        return render(request,'dashboard.html', { 'auth_credentials': credentials})

def google_analytics_accounts(request):
    client_id = request.GET.get('dashboardClientId')
    sessionCredentials = accounts_credentials(request,client_id)
    credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
    drive = build('analytics', 'v3', credentials=credentials)
    list_accounts = Profile.objects.get(user_id = request.session['_auth_user_id'])
    if list_accounts.accounts_data == "" and list_accounts.site_urls == "":
        accounts = drive.management().accounts().list().execute()
        data = json.dumps(accounts)
        consoleservice = build('webmasters', 'v3', credentials=credentials)
        site_list = consoleservice.sites().list().execute()
        sites = json.dumps(site_list)
        urls =  Profile.objects.filter(user_id = request.session['_auth_user_id']).update(accounts_data = data , site_urls = sites)
    lists = list_accounts.accounts_data
    data_list = json.loads(lists)
    return data_list['items']
    
def google_accounts(request):
    # ~ users = Companies.objects.get()
    client_id = request.GET.get('dashboardClientId')
    sessionCredentials = accounts_credentials(request,client_id).get("session_credentials")
    clientId = accounts_credentials(request,client_id).get("get_client_id")
    credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
    drive = build('analytics', 'v3', credentials=credentials)   
    list_accounts = Companies.objects.get(id = clientId)
    if list_accounts.accounts_data == "" and list_accounts.console_url == "" and list_accounts.profile_id is None:
        accounts = drive.management().accounts().list().execute()
        data = json.dumps(accounts)
        consoleservice = build('webmasters', 'v3', credentials=credentials)
        site_list = consoleservice.sites().list().execute()
        sites = json.dumps(site_list)
        urls =  Companies.objects.filter(id = clientId).update(accounts_data = data , console_url = sites)
        list_accounts = Companies.objects.get(id = clientId)
        data_list = list_accounts.accounts_data
        # ~ print(data_list['items'])
        data1 = json.loads(data_list)
        site_urls = list_accounts.console_url
        sites = json.loads(site_urls)
        verified_sites_urls = [s['siteUrl'] for s in sites['siteEntry']
                        if s['permissionLevel'] != 'siteUnverifiedUser'
                            and s['siteUrl'][:4] == 'http']
        if data1['items']:
            account = data1['items'][3].get('id')
            properties = drive.management().webproperties().list(accountId=account).execute()
            if properties.get('items'):
                website_url = properties.get('items')[0].get('websiteUrl')
                if list_accounts.analytics_url == "":
                    analytics_url = Companies.objects.filter(id = clientId).update(analytics_url = website_url)
                elif list_accounts.analytics_url != website_url:
                    analytics_url = Companies.objects.filter(id = clientId).update(analytics_url = "")
                    ana_url = Companies.objects.filter(id = clientId).update(analytics_url = website_url)
                property = properties.get('items')[0].get('id')
                profiles = drive.management().profiles().list(accountId=account,webPropertyId=property).execute()
                if profiles.get('items'):
                    profileId = profiles.get('items')[0].get('id')
                    # ~ print(profileId)
                    Companies.objects.filter(id = clientId).update(profile_id = profileId)
                # return the first view (profile) id.
                    return { 'id': profileId}
    else:
        profileId = list_accounts.profile_id
        print(profileId)
        return { 'id': profileId}

def delete_leads(request):
    leadName = request.GET.get('leadName')
    start_date = request.GET.get('startDate')
    end_date = request.GET.get('endDate')
    client_id = request.GET.get('dashboardClientId')
    clientId = accounts_credentials(request,client_id).get("get_client_id")
    leadsAnalytics = AnalyticsLeads.objects.filter(company_id = clientId,lead_name = leadName).update(is_deleted = True)
    hiddenAnalytics = HiddenLeads(user_id = request.session['_auth_user_id'] , lead_name = leadName , remove_date = datetime.date(datetime.now()) , company_id = clientId)
    hiddenAnalytics.save()
    return redirect("/dashboard")
    
def undo(request):
    leadname = request.GET.get('name')
    start_date = datetime.date(datetime.now())-timedelta(days=30)
    end_date = datetime.date(datetime.now())
    client_id = request.GET.get('dashboardClientId')
    clientId = accounts_credentials(request,client_id).get("get_client_id")
    leadsAnalytics = AnalyticsLeads.objects.filter(company_id = clientId,lead_name = leadname).update(is_deleted = False)
    hiddenAnalytics = HiddenLeads.objects.filter( lead_name = leadname ).delete()
    return redirect("/settings")
    
def get_session(request):
    client_id = request.GET.get('dashboardClientId')
    clientId = accounts_credentials(request,client_id).get("get_client_id")
    start_date = request.GET.get('startDate')
    end_date = request.GET.get('endDate')
    session_analytics = AnalyticsSessions.objects.filter(lead_date__gte=start_date, company_id = clientId,lead_date__lte=end_date).aggregate(Sum('traffic'),Avg('bounce_rate'),Sum('new_users'))
    users_sessions={}
    users_sessions['sessions'] = session_analytics['traffic__sum']
    users_sessions['newUsers'] = session_analytics['new_users__sum']
    users_sessions['bounceRate'] = session_analytics['bounce_rate__avg']
    return JsonResponse(users_sessions,safe=False)
    
def get_leads(request):
    client_id = request.GET.get('dashboardClientId')
    clientId = accounts_credentials(request,client_id).get("get_client_id")
    start_date = request.GET.get('startDate')
    end_date = request.GET.get('endDate')
    
    leads_analytics = AnalyticsLeads.objects.filter(lead_date__gte=start_date, company_id = clientId,lead_date__lte=end_date,is_deleted = False).all()
    data_csv = []
    for Leads in leads_analytics:
        users_leads = []
        users_leads.append(Leads.id)
        users_leads.append(Leads.lead_name)
        users_leads.append(Leads.source)
        users_leads.append(Leads.country)
        users_leads.append(Leads.lead_date)
        data_csv.append(users_leads)
    return JsonResponse(data_csv,safe=False)
    
def get_leads_details(request):
    lead_name = request.GET.get('id')
    # ~ print(request.META)
    start_date = request.GET.get('startDate')
    end_date = request.GET.get('endDate')
    profile_id = google_accounts(request).get('id')
    client_id = request.GET.get('dashboardClientId')
    linkedIn_lead_name = lead_name.replace(" ", "-")
    linkedin_status = requests.get('https://www.linkedin.com/cws/company/insider?companyIdentifier=' + linkedIn_lead_name  ,allow_redirects = False)
    sessionCredentials = accounts_credentials(request,client_id).get("session_credentials")
    clientId = accounts_credentials(request,client_id).get("get_client_id")
    credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
    analytics = build('analytics', 'v4', credentials=credentials, discoveryServiceUrl = configuration.WHITELABEL_DISCOVERY_URI)
    if start_date is None and end_date is None:
        response = analytics.reports().batchGet(
              body={
                'reportRequests': [
                {
                  'viewId': profile_id,
                  'dateRanges': [{'startDate': "30daysAgo", 'endDate': "today"}],
                  'metrics': [{'expression': 'ga:sessionDuration'},{'expression': 'ga:bounces'}],
                  'dimensions':[{ 'name': 'ga:date' },{ 'name': 'ga:pageDepth' },{ 'name': 'ga:source' },{ 'name': 'ga:city' },{ 'name': 'ga:region' }],
                  "dimensionFilterClauses": [
                    {
                      "filters": [
                        {
                          "dimensionName": "ga:networkLocation",
                          "not": False,
                          "operator": "EXACT",
                          "expressions": [lead_name]
                        }
                      ]
                    }
                  ]
                }]
              }
          ).execute()

    else:
        response = analytics.reports().batchGet(
              body={
                'reportRequests': [
                {
                  'viewId': profile_id,
                  'dateRanges': [{'startDate': start_date, 'endDate': end_date}],
                  'metrics': [{'expression': 'ga:sessionDuration'},{'expression': 'ga:bounces'}],
                  'dimensions':[{ 'name': 'ga:date' },{ 'name': 'ga:pageDepth' },{ 'name': 'ga:source' },{ 'name': 'ga:city' },{ 'name': 'ga:region' }],
                  "dimensionFilterClauses": [
                    {
                      "filters": [
                        {
                          "dimensionName": "ga:networkLocation",
                          "not": False,
                          "operator": "EXACT",
                          "expressions": [lead_name]
                        }
                      ]
                    }
                  ]
                }]
              }
          ).execute()
    data_csv = []
    dimensions_data={}
    if len(response.get('reports')) > 0:
        header_row = []
        rep = response.get('reports')[0]
        metricHeaders = rep.get('columnHeader').get('metricHeader').get('metricHeaderEntries')
        dimensionHeaders = rep.get('columnHeader').get('dimensions')
        for dheader in dimensionHeaders:
            header_row.append(dheader.split(":")[1])
        dimensions_values=[]
        row_temp = []
        if rep.get('data').get('rowCount') > 0:
            rows = rep.get('data').get('rows', [])
            for row in rows:
                row_temp = []
                dimension_values = row.get('dimensions', [])
                metrics = row.get('metrics', [])
                for d in dimension_values:
                    row_temp.append(d)
                for m in metrics[0]['values']:
                    row_temp.append(m)
                dimension_values = row.get('dimensions', [])
                for d in dimension_values:
                    row_temp.append(d)
                data_csv.append(row_temp)
    return render(request,'get_leads_details.html', { 'auth_credentials': credentials, 'leads': data_csv , 'linkedin_status' : linkedin_status} )
    
def get_leads_data(request):
    lead_name = request.GET.get('lead_name')
    lead_date = request.GET.get('lead_date')
    start_date = request.GET.get('startDate')
    end_date = request.GET.get('endDate')
    profile_id = google_accounts(request).get('id')
    client_id = request.GET.get('dashboardClientId')
    sessionCredentials = accounts_credentials(request,client_id).get("session_credentials")
    clientId = accounts_credentials(request,client_id).get("get_client_id")
    credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
    analytics = build('analytics', 'v4', credentials=credentials, discoveryServiceUrl = configuration.WHITELABEL_DISCOVERY_URI)
    if start_date is None and end_date is None:
        response = analytics.reports().batchGet(
              body={
                'reportRequests': [
                {
                  'viewId': profile_id,
                  'dateRanges': [{'startDate': '30daysAgo', 'endDate': 'today'}],
                  'metrics': [{'expression': 'ga:timeOnPage'}],
                  'dimensions':[{ 'name': 'ga:pagePath' }],
                  "dimensionFilterClauses": [
                    { "operator": "AND",
                      "filters": [
                        {
                          "dimensionName": "ga:networkLocation",
                          "operator": "EXACT",
                          "expressions": [lead_name ]
                        },
                        {
                          "dimensionName": "ga:date",
                          "operator": "EXACT",
                          "expressions": [lead_date]
                        }
                      ]
                    }
                  ]
                }]
              }
          ).execute()
    else:
        response = analytics.reports().batchGet(
              body={
                'reportRequests': [
                {
                  'viewId': profile_id,
                  'dateRanges': [{'startDate': start_date, 'endDate': end_date}],
                  'metrics': [{'expression': 'ga:timeOnPage'}],
                  'dimensions':[{ 'name': 'ga:pagePath' }],
                  "dimensionFilterClauses": [
                    { "operator": "AND",
                      "filters": [
                        {
                          "dimensionName": "ga:networkLocation",
                          "operator": "EXACT",
                          "expressions": [lead_name ]
                        },
                        {
                          "dimensionName": "ga:date",
                          "operator": "EXACT",
                          "expressions": [lead_date]
                        }
                      ]
                    }
                  ]
                }]
              }
          ).execute()
    data_csv = []
    dimensions_data={}
    if len(response.get('reports')) > 0:
        header_row = []
        rep = response.get('reports')[0]
        metricHeaders = rep.get('columnHeader').get('metricHeader').get('metricHeaderEntries')
        dimensionHeaders = rep.get('columnHeader').get('dimensions')
        for dheader in dimensionHeaders:
            header_row.append(dheader.split(":")[1])
        dimensions_values=[]
        row_temp = []
        if rep.get('data').get('rowCount') > 0:
            rows = rep.get('data').get('rows', [])
            for row in rows:
                row_temp = []
                dimension_values = row.get('dimensions', [])
                metrics = row.get('metrics', [])
                for d in dimension_values:
                    row_temp.append(d)
                for m in metrics[0]['values']:
                    row_temp.append(m)
                dimension_values = row.get('dimensions', [])
                for d in dimension_values:
                    row_temp.append(d)
                data_csv.append(row_temp)
    return JsonResponse(data_csv,safe=False)
    
def get_channels(request):
    client_id = request.GET.get('dashboardClientId')
    clientId = accounts_credentials(request,client_id).get("get_client_id")
    start_date = request.GET.get('startDate')
    end_date = request.GET.get('endDate')
    channels_analytics = AnalyticsChannels.objects.filter(lead_date__gte=start_date, company_id = clientId,lead_date__lte=end_date).values('channels').annotate(Sum('channels'),Sum('users'),Sum('sessions'))
    data = []
    temp = 1;
    for channels in channels_analytics:
        users_channels = []
        users_channels.append(channels['channels'])
        users_channels.append(channels['users__sum'])
        users_channels.append(channels['sessions__sum'])
        users_channels.insert(0,temp)
        temp += 1
        data.append(users_channels)
    return JsonResponse(data,safe=False) 
    
def get_sources(request):
    client_id = request.GET.get('dashboardClientId')
    clientId = accounts_credentials(request,client_id).get("get_client_id")
    start_date = request.GET.get('startDate')
    end_date = request.GET.get('endDate')
    sources_analytics = AnalyticsSources.objects.filter(lead_date__gte=start_date, company_id = clientId,lead_date__lte=end_date).values('sources').annotate(Sum('sources'),Sum('users'),Sum('sessions'))
   
    data = []
    temp = 1;
    for sources in sources_analytics:
        users_sources = []
        users_sources.append(sources['sources'])
        users_sources.append(sources['users__sum'])
        users_sources.append(sources['sessions__sum'])
        users_sources.insert(0,temp)
        temp += 1
        data.append(users_sources)
    return JsonResponse(data,safe=False)
    
        
def settings(request): 
    client_id = request.GET.get('client_id')
    sessionCredentials = accounts_credentials(request,client_id).get("session_credentials")
    clientId = accounts_credentials(request,client_id).get("get_client_id")
    start_date = datetime.date(datetime.now())-timedelta(days=30)
    end_date = datetime.date(datetime.now())
    hiddenLeads = HiddenLeads.objects.filter(company_id = clientId).all()
    if sessionCredentials == None:
        return render(request,'profile.html',{ 'auth_credentials': sessionCredentials})
    else:
        credentials = google.oauth2.credentials.Credentials(**sessionCredentials)
        companyWebsites = Companies.objects.filter(id=clientId).get()
        user_company = CompanyUsers.objects.filter(user_id = request.session['_auth_user_id']).all()
        sites = json.loads(companyWebsites.console_url)
        verified_sites_urls = [s['siteUrl'] for s in sites['siteEntry']
                        if s['permissionLevel'] != 'siteUnverifiedUser'
                            and s['siteUrl'][:4] == 'http']
        google_analytics_account = companyWebsites.analytics_url
        google_search_console = verified_sites_urls[0]
        form = profileForm(instance=request.user)
        return render(request,'profile.html' ,{'form': form , 'google_analytics_account':google_analytics_account,'google_search_console':google_search_console, 'hiddenLeads' : hiddenLeads}) 
        

def add_client(request):
    if request.method == 'POST':
        client_data = request.POST.get("formData")
        company = Companies(agency_id = request.session['_auth_user_id'] , company_name = client_data )
        company.save()
        company_user = CompanyUsers(user_id = request.session['_auth_user_id'] , company_id = company.id )
        company_user.save()
        messages.success(request, 'Client added successfully.')
        success= { 'success':'Client added successfully'}
        return JsonResponse(success)
        
def weekly_email_summaries(request):
    if request.method == 'POST':
        companyuser_id = request.POST.get("companyuser_id")
        notify_weekly = request.POST.get("notify_weekly")
        try:
            emailSummary = WeeklyEmailSummary.objects.get(company_user_id = companyuser_id)
            WeeklyEmailSummary.objects.filter(company_user_id = companyuser_id).update(notify_weekly = notify_weekly)
        except WeeklyEmailSummary.DoesNotExist:
            email_summary = WeeklyEmailSummary(company_user_id = companyuser_id, notify_weekly = notify_weekly)
            email_summary.save()
        success= { 'success':'Email summary for this email is subscribed By you'}
        return JsonResponse(success)
        
def invite_user(request):
    client_username = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(10))
    client_name = request.POST.get("client_name")
    client_email = request.POST.get("client_email")
    client_company = request.POST.get("client_company")
    data = json.loads(client_company)
    isError=False
    errors={}
    if(User.objects.filter(email=client_email)).exists():
        isError=True
        errors['email']= 'Email already exists.'
    
    if(isError):
        return JsonResponse(errors)
    else:  
        user= User(username = client_username,first_name = client_name,  email=client_email)
        password = User.objects.make_random_password()
        user.set_password(password)
        user.save()
        profile = Profile.objects.filter(user_id=user.id).update(is_company=True,agency_id=request.session['_auth_user_id'])
        for client in data:
            company = CompanyUsers(user_id=user.id,company_id= client)
            company.save()
        subject = 'Invite Email'
        message = render_to_string('email_templates/invitation_email.html', {
            'user': user,
            'client_email': client_email,
            'password': password,
        })
        user.email_user(subject, message)
        messages.success(request, 'User has been invited successfully.')
        success= { 'success':'User has been invited successfully'}
        return JsonResponse(success)
        
def edit_invited_user(request):
    client_id = request.GET.get("id")
    if request.method == 'POST':
        client_company = request.POST.get("edit_client_company")
        client_id = request.POST.get("edit_client_id")
        data = json.loads(client_company)
        results = list(map(int,data))
        company = CompanyUsers.objects.filter(user_id = client_id).all()
        users = []
        # ~ difference = Diff(user_list,data_list)
        for comp in company:
            company_id = comp.company_id
            users.append(company_id)
        if len(users) >= len(results):
            difference = Diff(users,results)
            for diff in difference:
                CompanyUsers.objects.filter(user_id = client_id , company_id = diff).delete() 
        elif len(results) >= len(users):
            difference = Diff(results,users)
            for diff in difference:
                usercompany = CompanyUsers(user_id = client_id , company_id = diff)
                usercompany.save()
        success= { 'success':'User has been updated successfully'}
        return JsonResponse(success,safe=False)
        
    users =  Profile.objects.get(user_id = client_id)
    company_users =  CompanyUsers.objects.filter(user_id = client_id).all()
    userCompany = []
    users_profile = {}
    users_profile['first_name'] = users.user.first_name
    users_profile['email'] = users.user.email
    for comp in company_users:
        company_id = comp.company_id
        userCompany.append(company_id)
    users_profile['company_id'] = userCompany
    return JsonResponse(users_profile,safe=False)
    
def delete_invited_user(request):
    profile_id = request.GET.get("id")
    Delete_user = Profile.objects.filter(user_id = profile_id).get()
    Delete_user.user.delete()
    delete_companies_user = CompanyUsers.objects.filter(user_id = profile_id).all().delete()
    messages.success(request, 'User has been deleted successfully.')
    return redirect('/settings')
    
def Diff(user_list,data_list):
    return(list(set(user_list)-set(data_list)))
        
def edit_profile(request):
    form = profileForm(data=request.POST, instance=request.user)
    if form.is_valid():
        update = form.save(commit=False)
        update.user = request.user
        update.save()
        messages.success(request, 'Profile has been updated successfully.')
        return redirect('/settings')
    else:
        return render(request,'profile.html' ,{'form': form }) 
    
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
       
        if form.is_valid():
            update = form.save()
            update_session_auth_hash(request,form.user)
            update.save()
            return redirect('/settings')
    else:
        form = PasswordChangeForm(user=request.user)
        return render(request,'change_password.html' ,{'form': form }) 
    

def get_querries(request):
    client_id = request.GET.get('dashboardClientId')
    clientId = accounts_credentials(request,client_id).get("get_client_id")
    start_date = request.GET.get('startDate')
    end_date = request.GET.get('endDate')
    querries_analytics = AnalyticsQuerries.objects.filter(lead_date__gte=start_date, company_id = clientId,lead_date__lte=end_date).values('querries').annotate(Sum('querries'),Sum('impressions'),Sum('clicks'),Sum('ctr'),Avg('position'))
    data_query = []
    for search_console in querries_analytics:
        users_querries = []
        users_querries.append(search_console['querries'])
        users_querries.append(search_console['position__avg'])
        users_querries.append(search_console['impressions__sum'])
        users_querries.append(search_console['ctr__sum'])
        users_querries.append(search_console['clicks__sum'])
        data_query.append(users_querries)
        
    pages_analytics = AnalyticsPages.objects.filter(lead_date__gte=start_date, company_id = clientId,lead_date__lte=end_date).values('pages').annotate(Sum('pages'),Sum('impressions'),Sum('clicks'),Sum('ctr'),Avg('position'))
    data_page = []
    for search_console in pages_analytics:
        users_pages = []
        users_pages.append(search_console['pages'])
        users_pages.append(search_console['position__avg'])
        users_pages.append(search_console['impressions__sum'])
        users_pages.append(search_console['ctr__sum'])
        users_pages.append(search_console['clicks__sum'])
        data_page.append(users_pages)
        
    countries_analytics = AnalyticsCountries.objects.filter(lead_date__gte=start_date, company_id = clientId,lead_date__lte=end_date).values('countries').annotate(Sum('countries'),Sum('impressions'),Sum('clicks'),Sum('ctr'),Avg('position'))
    data_country = []
    for search_console in countries_analytics:
        users_countries = []
        users_countries.append(search_console['countries'])
        users_countries.append(search_console['position__avg'])
        users_countries.append(search_console['impressions__sum'])
        users_countries.append(search_console['ctr__sum'])
        users_countries.append(search_console['clicks__sum'])
        data_country.append(users_countries)
        
    devices_analytics = AnalyticsDevices.objects.filter(lead_date__gte=start_date, company_id = clientId,lead_date__lte=end_date).values('devices').annotate(Sum('devices'),Sum('impressions'),Sum('clicks'),Sum('ctr'),Avg('position'))
    data_device = []
    for search_console in devices_analytics:
        users_devices = []
        users_devices.append(search_console['devices'])
        users_devices.append(search_console['position__avg'])
        users_devices.append(search_console['impressions__sum'])
        users_devices.append(search_console['ctr__sum'])
        users_devices.append(search_console['clicks__sum'])
        data_device.append(users_devices)
                        
    return JsonResponse({ 'querries': data_query , 'pages' : data_page, 'countries' : data_country, 'devices' : data_device},safe=False)


def credentials_to_dict(credentials):
    return {'token': credentials.token,
          'refresh_token': credentials.refresh_token,
          'token_uri': credentials.token_uri,
          'client_id': credentials.client_id,
          'client_secret': credentials.client_secret,
          'scopes': credentials.scopes}
def deauthorize(request):
    client_id = request.GET.get('id')
    company_credentials = Companies.objects.get(id=client_id)
    credentials = json.loads(company_credentials.credentials)
    start_date = datetime.date(datetime.now())-timedelta(days=30)
    end_date = datetime.date(datetime.now())
    sessionCredentials = google.oauth2.credentials.Credentials(**credentials)
    # ~ tokens = credentials['token']
    revoked = requests.post('https://accounts.google.com/o/oauth2/revoke',params={'token': sessionCredentials.refresh_token},headers = {'content-type': 'application/x-www-form-urlencoded'})
    com = Companies.objects.filter(id=client_id).update(credentials = "" , console_url = "" , accounts_data = "" , analytics_url ="",profile_id = None,last_fetched = None )
    session_analytics = AnalyticsSessions.objects.filter(company_id = client_id).delete()
    leads_analytics = AnalyticsLeads.objects.filter(company_id = client_id).delete()
    channels_analytics = AnalyticsChannels.objects.filter(company_id = client_id).delete()
    sources_analytics = AnalyticsSources.objects.filter(company_id = client_id).delete()
    querries_analytics = AnalyticsQuerries.objects.filter(company_id = client_id).delete()
    pages_analytics = AnalyticsPages.objects.filter(company_id = client_id).delete()
    countries_analytics = AnalyticsCountries.objects.filter(company_id = client_id).delete()
    devices_analytics = AnalyticsDevices.objects.filter(company_id = client_id).delete()
    messages.success(request, 'Google Authorization is removed successfully')
    return redirect("/settings")
    
def authorize(request):
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(configuration.WHITELABEL_SECRET_FILE, scopes=configuration.WHITELABEL_GOOGLE_SCOPES)
    flow.redirect_uri = request.build_absolute_uri("/oauthCallback")
    query = request.GET.get("id")
    authorization_url, state = flow.authorization_url(
    access_type='offline',
    include_granted_scopes='true',
    state = query)
    request.session['state'] = state
    
    return redirect(authorization_url)
    
def oauthCallback(request):
    state = request.session['state']
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(configuration.WHITELABEL_SECRET_FILE, scopes=configuration.WHITELABEL_GOOGLE_SCOPES, state=state)
    flow.redirect_uri = request.build_absolute_uri("oauthCallback")
    
    # Use the authorization server's response to fetch the OAuth 2.0 tokens.
    authorization_response = request.build_absolute_uri()
    
    flow.fetch_token(authorization_response=authorization_response)
    # Store credentials in the session.
    # ACTION ITEM: In a production app, you likely want to save these
    #              credentials in a persistent database instead.
    credentials = flow.credentials
    if credentials_to_dict(credentials)['refresh_token'] == None:
        messages.error(request, 'The selected Google account is already associated with a different WhiteLabelVisId account.')
        context = {'active_tab':'tabs-icons-text-5'}
        return redirect("/settings" ,{'active_tab':'tabs-icons-text-5'} )
    else:
        company_session = credentials_to_dict(credentials)
        data = json.dumps(company_session)
        company_credentials = Companies.objects.filter(id=state).update(credentials=data)
        company = Companies.objects.get(id = state)
        collectCompanyData(company ,datetime.date(datetime.now())-timedelta(days=30) , datetime.date(datetime.now())-timedelta(days=1))
    return redirect("/dashboard")
    
# ~ def add_client(request):
    # ~ return render(request,'add_client.html')
    
def clients(request):
    return render(request,'clients.html')
    
def logout(request):
    signout(request)
    return redirect('/login')
    
def signup(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        #~ print(form)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.profile.is_agency = True
            user.save()
            current_site = get_current_site(request)
            subject = 'Activate Your Account'
            message = render_to_string('email_templates/account_activation_email.html', {
                'user': user,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)
            if request.user.is_authenticated == True and request.user.is_superuser == True:
                return redirect('/admin/auth/user')
            else:
                return redirect('account_activation_sent')
            
            
    else:
        form = CustomUserCreationForm()
    return render(request, 'registration/signup.html', {'form': form})


def account_activation_sent(request):
    messages.success(request, 'Please confirm your email address to complete the registration.')
    return redirect('/login')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
        # ~ print (User)   
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
        
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        login(request)
        messages.success(request, 'Your Account has been activated.')
        return redirect('/login')
    else:
        messages.error(request, 'The confirmation link was invalid, possibly because it has already been used.')
        return redirect('/login')

