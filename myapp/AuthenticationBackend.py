from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User

class EmailAuthBackend(ModelBackend):
    """
    Authenticate against django.contrib.auth.models.User using
    e-mail address instead of username.
    """
    def authenticate(self, username=None, password=None):
        try:
            user = User.objects.get(email__iexact = username)
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            return None


#~ from django.contrib.auth.models import User



#~ class EmailAuthBackend(object):
    #~ def authenticate(self,request,username, password):
        #~ try:
            #~ user = User.objects.get(email=username)
            #~ success = user.check_password(password)
            #~ if success:
                #~ return user
        #~ except User.DoesNotExist:
            #~ pass
        #~ return None 
        
        #~ if get_attr(user,'is_active') and user.check_password(password):
            #~ return user
        #~ return None    
    
    def get_user(self,uid):
        try:
            return User.objects.get(pk-uid)
        except:
            return None    
