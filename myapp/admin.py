from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User,Group
from django.contrib.auth.forms import UserCreationForm

from .models import Profile

admin.site.unregister(Group)

class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'
    
class UserCreateForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('email', 'first_name')


class UserAdmin(UserAdmin):
    add_form = UserCreateForm
    prepopulated_fields = {'email': ('first_name', )}

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('first_name', 'email', 'password1', 'password2', ),
        }),
    )


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
