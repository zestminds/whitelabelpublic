from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User, AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
from django.core.serializers.json import DjangoJSONEncoder


#~ class Agency(AbstractUser):
   #~ email_confirmed = models.BooleanField(default=False)
   #~ is_agency = models.BooleanField(default=False)
   #~ is_company = models.BooleanField(default=False)
   #~ is_user = models.BooleanField(default=False)

class Analytics(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    analytics_data = models.TextField(max_length=500, blank=True)
    analytics_profile_id = models.IntegerField(null=True,blank=True)
    
    
class Siteurls(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    accounts_data = models.TextField(max_length=500, blank=True)
    site_urls = models.TextField(max_length=500, blank=True)
    
class Companies(models.Model):
    company_name = models.TextField(max_length=500, blank=True)
    credentials = models.TextField(max_length=500, blank=True)
    agency_id = models.IntegerField(null=True ,blank=True)
    profile_id = models.CharField(max_length=30, blank=True,null=True)
    analytics_website = models.TextField(max_length=500, blank=True)
    console_url = models.TextField(max_length=500, blank=True)
    analytics_url = models.TextField(max_length=500, blank=True)
    accounts_data = models.TextField(max_length=500, blank=True)
    last_fetched = models.DateField(null=True, blank=True)
    is_primary = models.BooleanField(default=False)
    # ~ analytics_profile_id = models.IntegerField(null=True,blank=True)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # ~ company = models.ForeignKey(Companies, on_delete=models.CASCADE, blank=True)
    bio = models.TextField(max_length=500, blank=True)
    location = models.CharField(max_length=30, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    email_confirmed = models.BooleanField(default=False)
    is_agency = models.BooleanField(default=False)
    is_company = models.BooleanField(default=False)
    is_user = models.BooleanField(default=False)
    tokens = models.TextField(max_length=500, blank=True)
    accounts_data = models.TextField(max_length=500, blank=True)
    site_urls = models.TextField(max_length=500, blank=True)
    agency_id = models.IntegerField(null=True ,blank=True)
    
class CompanyUsers(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,null=True)
    # ~ user = models.ManyToManyField(User)
    # ~ company = models.ManyToManyField(Companies)
    company = models.ForeignKey(Companies, on_delete=models.CASCADE,null=True)
    
class AnalyticsSessions(models.Model):
    company = models.ForeignKey(Companies, on_delete=models.CASCADE,null=True)
    traffic = models.IntegerField(blank=False)
    new_users = models.IntegerField(blank=False)
    bounce_rate = models.DecimalField(decimal_places=2,max_digits=5)
    lead_date = models.DateField(blank=False,null=True)
    
class AnalyticsLeads(models.Model):
    company = models.ForeignKey(Companies, on_delete=models.CASCADE,null=True)
    lead_name = models.CharField(max_length=100, blank=False)
    source = models.CharField(max_length=50, blank=False)
    country = models.CharField(max_length=50, blank=False)
    lead_date = models.DateField(blank=False)
    is_deleted = models.BooleanField(default=False)
    
class HiddenLeads(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE,null=True)
    lead_name = models.CharField(max_length=100, blank=False)
    company = models.ForeignKey(Companies, on_delete=models.CASCADE,null=True)
    remove_date = models.DateField(null=True, blank=True)
    
class WeeklyEmailSummary(models.Model):
    company_user = models.OneToOneField(CompanyUsers, on_delete=models.CASCADE,primary_key=True)
    notify_weekly = models.BooleanField(default=False)
    
class AnalyticsChannels(models.Model):
    company = models.ForeignKey(Companies, on_delete=models.CASCADE,null=True)
    channels = models.CharField(max_length=50, blank=False)
    users = models.IntegerField(blank=False)
    sessions = models.IntegerField(blank=False)
    lead_date = models.DateField(blank=False,null=True)
    
class AnalyticsSources(models.Model):
    company = models.ForeignKey(Companies, on_delete=models.CASCADE,null=True)
    sources = models.CharField(max_length=50, blank=False)
    users = models.IntegerField(blank=False)
    sessions = models.IntegerField(blank=False)
    lead_date = models.DateField(blank=False,null=True)
    
    
class AnalyticsQuerries(models.Model):
    company = models.ForeignKey(Companies, on_delete=models.CASCADE,null=True)
    lead_date = models.DateField(blank=False,null=True)
    impressions = models.IntegerField(blank=False,null=True)
    clicks = models.IntegerField(blank=False,null=True)
    position = models.DecimalField(decimal_places=2,max_digits=5,null=True)
    ctr = models.DecimalField(decimal_places=2,max_digits=5,null=True)
    querries = models.CharField(max_length=100, blank=False,null=True)
    
class AnalyticsPages(models.Model):
    company = models.ForeignKey(Companies, on_delete=models.CASCADE,null=True)
    lead_date = models.DateField(blank=False,null=True)
    impressions = models.IntegerField(blank=False)
    clicks = models.IntegerField(blank=False)
    position = models.DecimalField(decimal_places=2,max_digits=5,null=True)
    ctr = models.DecimalField(decimal_places=2,max_digits=5,null=True)
    pages = models.CharField(max_length=100, blank=False)
    
class AnalyticsCountries(models.Model):
    company = models.ForeignKey(Companies, on_delete=models.CASCADE,null=True)
    lead_date = models.DateField(blank=False,null=True)
    impressions = models.IntegerField(blank=False)
    clicks = models.IntegerField(blank=False)
    position = models.DecimalField(decimal_places=2,max_digits=5,null=True)
    ctr = models.DecimalField(decimal_places=2,max_digits=5,null=True)
    countries = models.CharField(max_length=100, blank=False)
    
class AnalyticsDevices(models.Model):
    company = models.ForeignKey(Companies, on_delete=models.CASCADE,null=True)
    lead_date = models.DateField(blank=False,null=True)
    impressions = models.IntegerField(blank=False)
    clicks = models.IntegerField(blank=False)
    position = models.DecimalField(decimal_places=2,max_digits=5,null=True)
    ctr = models.DecimalField(decimal_places=2,max_digits=5,null=True)
    devices = models.CharField(max_length=100, blank=False)
    
class Providers(models.Model):
    provider_name = models.TextField(max_length=500, blank=True)


@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
