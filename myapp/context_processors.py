from myapp.models import Companies,CompanyUsers
from django.contrib.auth.models import User
from django.db.models import Max
def companies(request):
    if '_auth_user_id' not in request.session:
        users = None
        clients_companies = None
        Companies = None
    else:
        Companies = CompanyUsers.objects.filter(user_id = request.session['_auth_user_id']).all()
        clients_companies = CompanyUsers.objects.filter(user_id = request.session['_auth_user_id']).all().values_list('company_id', flat=True)
        related_users =CompanyUsers.objects.filter(company_id__in = clients_companies).values_list('user_id',flat=True)
        users= User.objects.filter(id__in = related_users).prefetch_related('companyusers_set')
    return {'users': users,"clients_companies": clients_companies,"Companies" : Companies}
