from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
#~ from django.contrib.auth import get_user_model
from django import forms
from django.core.exceptions import ValidationError
from django.contrib.auth import password_validation
from django.contrib.auth.hashers import make_password
import random
import string
class CustomUserCreationForm(forms.Form):
    first_name = forms.CharField(min_length=4, max_length=150,widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Name'}))
    email = forms.EmailField(label='Enter email',widget=forms.TextInput(attrs={'class': 'form-control','placeholder':'Email'}))
    password1 = forms.CharField(label='Enter password', widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder':'Password'})) 
    password2 = forms.CharField(label='Enter Confirm password', widget=forms.PasswordInput(attrs={'class': 'form-control','placeholder':'Password Confirmation'}))
    terms = forms.BooleanField(initial=False,error_messages={"required": 'You must accept the terms and conditions'},widget=forms.CheckboxInput(attrs={'class': 'form-control'}))
    def clean_username(self):
        # ~ username = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(10))
        username = self.cleaned_data['username'].lower()
        # ~ r = User.objects.filter(username=username)
        # ~ if r.count():
            # ~ raise  ValidationError("Username already exists")
        return username
        
    def clean_first_name(self):
        first_name = self.cleaned_data['first_name']
        return first_name
 
    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise  ValidationError("Email already exists")
        return email
 
    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        try:
            password_validation.validate_password(password1)
        except forms.ValidationError as error:

            self.add_error('password1', error)
        return password1
  
        #~ if len(password1) < 8:
            #~ raise ValidationError("Password should be minimum 8 characters long")
        #~ else:
            #~ raise password_validation.validate_password(password1)
        #~ return password1
            
    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')
        if len(password2) < 8 :
            raise ValidationError("Password should be minimum 8 characters long")
        if password1 and password2 and password1 != password2:
            raise ValidationError("Password don't match")    
        return password2
    def save(self, commit=True):
        user = User.objects.create_user(
            # ~ username = self.cleaned_data[usersname],
            ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(10)),
            first_name=self.cleaned_data['first_name'],
            email=self.cleaned_data['email'],
            password=self.cleaned_data['password1']
        )
        return user
class CustomInviteUserForm(forms.Form):
    # ~ def __init__(self, *args, **kwargs):
            # ~ super(UserCreationForm, self).__init__(*args, **kwargs)
            # ~ self.fields['password1'].required = False
            # ~ self.fields['password2'].required = False
            # ~ # If one field gets autocompleted but not the other, our 'neither
            # ~ # password or both password' validation will be triggered.
            # ~ self.fields['password1'].widget.attrs['autocomplete'] = 'off'
            # ~ self.fields['password2'].widget.attrs['autocomplete'] = 'off'
     
    class Meta:  
        model = User  
        fields = ['username','first_name',  'email']
    

class profileForm(forms.ModelForm):
    
    class Meta:
        model = User
        fields = ['first_name',  'email']
        
        # ~ extra_kwargs = {
            # ~ "password": {"write_only": True},
        # ~ }

    
    # ~ def create(self, validated_data):
        # ~ user = User.objects.create(
            # ~ email=validated_data['email'],
            # ~ username=validated_data['username'],
            # ~ password = make_password(validated_data['password'])
        # ~ )
        # ~ user.save()
        # ~ return user
