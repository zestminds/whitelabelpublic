$(document).ready(function() {
		var dashboardClientId = $(".dashboard_clients").val();
		var endDate = "";
		var startDate = "";
		var start_date = new Date();
		start_date.setDate(start_date.getDate() - 30)
		var end_date = new Date();
		startDate = convertDate(start_date)
		endDate = convertDate(end_date);
		function convertDate(date) {
		  var yyyy = date.getFullYear().toString();
		  var mm = (date.getMonth()+1).toString();
		  var dd  = date.getDate().toString();

		  var mmChars = mm.split('');
		  var ddChars = dd.split('');

		  return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
		}
		$('#date_range').find('.start_date').val(startDate);
		$('#date_range').find('.end_date').val(endDate);
		$("#loader").show();
		$("#dashboard_data").hide();
		//~ $('#loader')
            //~ .hide()
            //~ .ajaxStart(function() {
                //~ $(this).show();
            //~ })
            //~ .ajaxStop(function() {
                //~ $(this).hide();
            //~ });
		//~ $("#main").find('.dashboard').html('<div class="m-2 text-center"><h3>Loading...</h3></div>')
		var template_leads = "<tr id='ID'><td><input type='checkbox' class='chk_all_leads' name= 'leads' value='ID' /></td><td class='wordwrapp' scope='row'><a href='LEAD' data-toggle='modal' class='lead_id' id ='lead_name'>LEAD</a></td><td class='wordwrapp'>SOURCE</td><td class='wordwrapp'>COUNTRY</td><td class='wordwrapp'>DATE</td><td class='wordwrapp'><a href='javascript:void(0);' class='rm_row' data-val = 'ID'><i class='ni ni-basket'></i></a></td></tr>";
		var template_channels = "<tr id='ID'><td></td><td>SOURCE</td><td>USERS</td><td>SESSIONS</td></tr>";
		var template_sources = "<tr id='ID'><td></td><td>SOURCE</td><td>USERS</td><td>SESSIONS</td></tr>";
		
		$.ajax({
				url: '/get_session',
				type: 'get',
				data: {
					startDate: startDate,
					endDate : endDate,
					dashboardClientId: dashboardClientId
				},
				success: function(response){
				
					$('#dashboard_traffic').find('.traffic').html(response.sessions);
					$('#dashboard_traffic').find('.bounce_rate').html(financial(response.bounceRate));
					$('#dashboard_traffic').find('.users').html(response.newUsers);
					//~ traffic
				},
				error: function(er){
					console.log(er)
				}
		});
		function financial(x) {
		  return Number.parseFloat(x).toFixed(2);
		}
		
		function querryctr(x) {
		  return Number.parseFloat(x).toFixed(1);
		}
		$.ajax({
				url: '/get_leads',
				type: 'get',
				data: {
					startDate: startDate,
					endDate : endDate,
					dashboardClientId: dashboardClientId
				},
				success: function(response){
					$("#loader").hide();
					$("#dashboard_data").show();
					var table_leads = $('#data_lead').dataTable({
						
						//~ "processing": true,
						"searching": false,
						"bLengthChange" : false, //thought this line could hide the LengthMenu
						"bInfo":false,
						"pageLength": 8,
						//~ "fnDrawCallback": function(oSettings) {
							//~ if ($('#data_lead tr').length < 8) {
								//~ $('#data_lead_paginate').hide();
							//~ }
						//~ },
						"language": {
							"oPaginate": {
								"sNext": '<i class="fa fa-angle-right"></i>',
								"sPrevious": '<i class="fa fa-angle-left"></i>',
								"sFirst": '<i class="fa fa-step-backward"></i>',
								"sLast": '<i class="fa fa-step-forward"></i>' 
							}
							  
						} ,
                        'columnDefs': [{
							 'targets': 0,
							 'searchable': false,
							 'width': '1%',
							 'className': 'dt-body-center',
							 'render': function (data, type, full, meta){
								 return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
								 
							}
						},
						{
							 'targets': 5,
							 'searchable': false,
							 'width': '1%',
							 'className': 'dt-body-center',
							 'render': function (data, type, full, meta){
								 return '<a href="" class="editor_remove"><i class="ni ni-basket"></i></a>';
							}
						},
						{
							 'targets': 1,
							 'searchable': false,
							 'width': '1%',
							 'className': 'dt-body-center',
							 'render': function (data, type, full, meta){
								 return '<a href="' + data + '" data-toggle="modal" class="lead_id leads_values" value = "' + data + '" id ="lead_name">' + data + '</a>';
							}
						},
						{
							 'targets': 4,
							 'searchable': false,
							 'className': 'dt-body-center dateprograms',
							  "render": function(data) {
									
									return data;
							}
						}
						
						],
						"order": [[4, 'desc']],
                        //~ 'columnDefs': [],
						
                        data: response,
                    });
                    var buttons = new $.fn.dataTable.Buttons(table_leads, {
						buttons: [
							{ 
							text: 'Export',
							extend: 'csv',
							'className': 'btn btn-primary btn-sm',
							init: function( api, node, config) {
							   $(node).removeClass('dt-button buttons-csv buttons-html5')
							}
							}
						//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
						]
					}).container().appendTo($('#buttons_leads'));
                    
					$('#data_lead').on('click', 'a.editor_remove', function (e) {
						e.preventDefault();
						var leadName = $(this).closest('tr').find('.leads_values').attr('href');
						$.ajax({
							url: '/delete_leads',
							type: 'get',
							data: {
								startDate: startDate,
								endDate : endDate,
								leadName : leadName
							},
							success: function(response){
								//~ $('a.editor_remove').parent("td").parent("tr").hide();
							},
							error: function(er){
								console.log(er)
							}
						});
						$(this).parent("td").parent("tr").remove();
						//$(this).parent("td").parent("tr").remove();
						//table.remove(this).draw( false );
						/*table.remove( $(this).parent("td").parent("tr"), {
							title: 'Delete record',
							message: 'Are you sure you wish to remove this record?',
							buttons: 'Delete'
						} );*/
					} );
				},
				error: function(er){
					console.log(er)
				}
		});
		 var getdateformat = function(val) {
			val = val.toString();
			//~ var temp = val[0]+val[1]+val[2]+val[3]+"-"+val[4]+val[5]+"-"+val[6]+val[7];
			var temp = val[6]+val[7]+"-"+val[4]+val[5]+"-"+val[0]+val[1]+val[2]+val[3];
			return temp;
		};
		
		$.ajax({
				url: '/get_querries',
				type: 'get',
				data: {
					startDate: startDate,
					endDate : endDate,
					dashboardClientId: dashboardClientId
				},
				success: function(response){
					$('#dashboard_traffic').find('.avg_position').html(financial(response["pages"][0][1]));
					var table_querry = $('#data_querries').dataTable({
						//~ "processing": true,
						"searching": false,
						"bLengthChange" : false, //thought this line could hide the LengthMenu
						"bInfo":false,
						"pageLength": 9,
						"language": {
							"oPaginate": {
								"sNext": '<i class="fa fa-angle-right"></i>',
								"sPrevious": '<i class="fa fa-angle-left"></i>',
								"sFirst": '<i class="fa fa-step-backward"></i>',
								"sLast": '<i class="fa fa-step-forward"></i>' 
							}
							  
						} , 
						columnDefs: [
							{ 
								targets: 1,
								render: function ( data, type, row ) {
									//~ console.log(data);
									return (querryctr(data));
								}
							},
							{ 
								targets: 3,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							}
						],
						//~ "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
						   //~ var index = iDisplayIndex +1;
						   //~ $('td:eq(0)',nRow).html(index);
						   //~ return nRow;
						//~ },
						data: response['querries'],
					 });
						var buttons = new $.fn.dataTable.Buttons(table_querry, {
							buttons: [
								{ 
								text: 'Export',
								extend: 'csv',
								'className': 'btn btn-primary btn-sm',
								init: function( api, node, config) {
								   $(node).removeClass('dt-button buttons-csv buttons-html5')
								}
								}
							//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
							]
						}).container().appendTo($('#buttons_querry'));
					
					var table_websites = $('#data_website').dataTable({
						"processing": true,
						"searching": false,
						"bLengthChange" : false, //thought this line could hide the LengthMenu
						"bInfo":false,
						"pageLength": 9,
						"language": {
							"oPaginate": {
								"sNext": '<i class="fa fa-angle-right"></i>',
								"sPrevious": '<i class="fa fa-angle-left"></i>',
								"sFirst": '<i class="fa fa-step-backward"></i>',
								"sLast": '<i class="fa fa-step-forward"></i>' 
							}
							  
						} ,
						columnDefs: [
							{ 
								targets: 1,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							},
							{ 
								targets: 3,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							}
						],
						//~ "columns": [
							//~ { "data": "keys" },
							//~ { "data": "position" },
							//~ { "data": "impressions" },
							//~ { "data": "ctr" },
							//~ { "data": "clicks" }
							
						//~ ],
						
						data: response['pages'],
						
					 });
					 var buttons = new $.fn.dataTable.Buttons(table_websites, {
						buttons: [
							{ 
							text: 'Export',
							extend: 'csv',
							'className': 'btn btn-primary btn-sm',
							init: function( api, node, config) {
							   $(node).removeClass('dt-button buttons-csv buttons-html5')
							}
							}
						//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
						]
					}).container().appendTo($('#buttons_websites'));
					var table_countries = $('#data_countries').dataTable({
						"processing": true,
						"searching": false,
						"bLengthChange" : false, //thought this line could hide the LengthMenu
						"bInfo":false,
						"pageLength":9,
						"language": {
							"oPaginate": {
								"sNext": '<i class="fa fa-angle-right"></i>',
								"sPrevious": '<i class="fa fa-angle-left"></i>',
								"sFirst": '<i class="fa fa-step-backward"></i>',
								"sLast": '<i class="fa fa-step-forward"></i>' 
							}
							  
						} ,
						columnDefs: [
							{ 
								targets: 1,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							},
							{ 
								targets: 3,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							}
						],
						//~ "columns": [
							//~ { "data": "keys" },
							//~ { "data": "position" },
							//~ { "data": "impressions" },
							//~ { "data": "ctr" },
							//~ { "data": "clicks" }
							
						//~ ],
						data: response['countries'],
						
					 });
					 var buttons = new $.fn.dataTable.Buttons(table_countries, {
						buttons: [
							{ 
							text: 'Export',
							extend: 'csv',
							'className': 'btn btn-primary btn-sm',
							init: function( api, node, config) {
							   $(node).removeClass('dt-button buttons-csv buttons-html5')
							}
							}
						//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
						]
					}).container().appendTo($('#buttons_countries'));
					var table_devices = $('#data_devices').dataTable({
						"processing": true,
						"searching": false,
						"bLengthChange" : false, //thought this line could hide the LengthMenu
						"bInfo":false,
						"pageLength": 9,
						"language": {
							"oPaginate": {
								"sNext": '<i class="fa fa-angle-right"></i>',
								"sPrevious": '<i class="fa fa-angle-left"></i>',
								"sFirst": '<i class="fa fa-step-backward"></i>',
								"sLast": '<i class="fa fa-step-forward"></i>' 
							}
							  
						} ,
						columnDefs: [
							{ 
								targets: 1,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							},
							{ 
								targets: 3,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							}
						],
						//~ "columns": [
							//~ { "data": "keys" },
							//~ { "data": "position" },
							//~ { "data": "impressions" },
							//~ { "data": "ctr" },
							//~ { "data": "clicks" }
							
						//~ ],
						data: response['devices'],
					 });
					 var buttons = new $.fn.dataTable.Buttons(table_devices, {
						buttons: [
							{ 
							text: 'Export',
							extend: 'csv',
							'className': 'btn btn-primary btn-sm',
							init: function( api, node, config) {
							   $(node).removeClass('dt-button buttons-csv buttons-html5')
							}
							}
						//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
						]
					}).container().appendTo($('#buttons_devices'));
				//~ console.log(response['countries'])
					//~ traffic
				},
				error: function(er){
					console.log(er)
				}
		});

		
		$.ajax({
				url: '/get_channels',
				type: 'get',
				data: {
					startDate: startDate,
					endDate : endDate,
					dashboardClientId: dashboardClientId
				},
				success: function(response){
					var table_channels= $('#data_channel').dataTable({
					//~ "processing": true,
					"searching": false,
					"bLengthChange" : false, //thought this line could hide the LengthMenu
					"bInfo":false,
					"bPaginate":false,
					"pageLength": 4,
					"language": {
						"oPaginate": {
							"sNext": '<i class="fa fa-angle-right"></i>',
							"sPrevious": '<i class="fa fa-angle-left"></i>',
							"sFirst": '<i class="fa fa-step-backward"></i>',
							"sLast": '<i class="fa fa-step-forward"></i>' 
						}
						  
					} ,
					data: response,
				 });
				var buttons = new $.fn.dataTable.Buttons(table_channels, {
					buttons: [
						{ 
						text: 'Export',
						extend: 'csv',
						'className': 'btn btn-primary btn-sm',
						init: function( api, node, config) {
						   $(node).removeClass('dt-button buttons-csv buttons-html5')
						}
						}
					//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
					]
				}).container().appendTo($('#buttons_channels'));
				},
				error: function(er){
					console.log(er)
				}
		});
		$.ajax({
				url: '/get_sources',
				type: 'get',
				data: {
					startDate: startDate,
					endDate : endDate,
					dashboardClientId: dashboardClientId
				},
				success: function(response){
					var table_sources = $('#data_scope').dataTable({
					//~ "processing": true,
					//editor_removeconsole.log(page.len())
					"searching": false,
					"bLengthChange" : false, //thought this line could hide the LengthMenu
					"bInfo":false,
					"pageLength": 5,
					"language": {
						"oPaginate": {
							"sNext": '<i class="fa fa-angle-right"></i>',
							"sPrevious": '<i class="fa fa-angle-left"></i>',
							"sFirst": '<i class="fa fa-step-backward"></i>',
							"sLast": '<i class="fa fa-step-forward"></i>' 
						}
						  
					} ,
					data: response,
					});
					var buttons = new $.fn.dataTable.Buttons(table_sources, {
						buttons: [
							{ 
							text: 'Export',
							extend: 'csv',
							'className': 'btn btn-primary btn-sm',
							init: function( api, node, config) {
							   $(node).removeClass('dt-button buttons-csv buttons-html5')
							}
							}
						//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
						]
					}).container().appendTo($('#buttons_source'));
				},
				error: function(er){
					console.log(er)
				}
		});
		//~ $(function(){
            //~ $("#lead_name").click(function(e) {
                //~ e.preventDefault();
                //~ $("#wrapper").toggleClass("toggled");
            //~ });

            //~ $(window).resize(function(e) {
              //~ if($(window).width()<=768){
                //~ $("#wrapper").removeClass("toggled");
              //~ }else{
                //~ $("#wrapper").addClass("toggled");
              //~ }
            //~ });
          //~ });
		
         $(document).on("click",'.lead_id',function(){
		   $('#exampleModal').find('.report_leads').html('<div class="m-2 text-center"><h3>Loading...</h3></div>');
           $('#exampleModal').modal('show');
		   var status_id = $(this).attr('href');
			$.ajax({
				url: '/get_leads_details',
				type: 'get',
				data: {
					id: status_id,
					dashboardClientId: dashboardClientId
					//~ startDate: startDate,
					//~ endDate : endDate
				},
				success: function(response){
				$('#exampleModal').find('.report_leads').html(response);
				$('#exampleModal').find('#currentLeadName').val(status_id);
				$('#exampleModal').find('.lead_title').html(status_id);
				},
				error: function(er){
					console.log(er)
				}
			});
			//~ var modal_data = $('#exampleModal').find('.report_leads')
			//~ modal_data.loads
			//~ console.log(here);
			//~ $('#exampleModal').modal('show');
           //~ alert(status_id); 
        });
        function clearLeadRows(that,leadId){
			var nextRow=that.closest('tr').next();
			if(nextRow && nextRow.hasClass('lead_'+leadId)){
				nextRow.remove();
			}else{
				return;
			}
			clearLeadRows(that,leadId);
		}
		function createLeadRows(that, rows, row_id){
			clearLeadRows(that,row_id);
			rows.forEach(function(rowData){
				that.closest('tr').after('<tr class="lead_per_sessions lead_'+row_id+'"><td></td><td>'+rowData[0]+'</td><td>'+rowData[1]+'</td><td></td><td></td></tr>');
			});
		}
		
		//~ $('#check_all').click(function() {
			//~ $('input[name=leads]').prop('checked', true);
		//~ });
		$("#check_all").change(function(){
		 var checked = $(this).is(':checked');
		 if(checked){
		   $(".chk_all_leads").each(function(){
			 $(this).prop("checked",true);
		   });
		 }else{
		   $(".chk_all_leads").each(function(){
			 $(this).prop("checked",false);
		   });
		 }
		});
	 
	  // Changing state of CheckAll checkbox 
		//~ $(".checkbox").click(function(){
			//~ if($(".checkbox").length == $(".checkbox:checked").length) {
			  //~ $("#checkall").prop("checked", true);
			//~ } else {
			  //~ $("#checkall").removeAttr("checked");
			//~ }

		//~ });
		//~ $('#check_all').click(function() {
			//~ $('input[name=leads]').prop('checked', false);
		//~ });
		
        $(document).on('click','.lead-record',function(e){
			var that=$(this);
			var lead_name = $('#exampleModal').find('#currentLeadName').val();
			var lead_date = that.data('leaddate');
			var row_id=lead_name.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'') + lead_date;
			clearLeadRows(that, row_id);
			that.closest('tr').after('<tr class="lead_per_sessions lead_'+row_id+'"><td colspan="5">Loading...</td></tr>')
			$.ajax({
				url: '/get_leads_data',
				type: 'get',
				data: {
					lead_name: lead_name,
					lead_date : lead_date,
					dashboardClientId: dashboardClientId
					//~ startDate: startDate,
					//~ endDate : endDate
				},
				success: function(response){
					createLeadRows(that,response,row_id);
				},
				error: function(er){
					console.log(er)
				}
			});
		});
		
		$(".date_range").change(function(){
			var startDate = "";
			var endDate = "";
			
			start_date = $("#startdate").val();
			var date    = new Date(start_date),
				startDate = convertDate(date);
			end_date = $("#enddate").val();
			var date    = new Date(end_date),
				endDate = convertDate(date);
			function convertDate(date) {
			  var yyyy = date.getFullYear().toString();
			  var mm = (date.getMonth()+1).toString();
			  var dd  = date.getDate().toString();

			  var mmChars = mm.split('');
			  var ddChars = dd.split('');

			  return yyyy + '-' + (mmChars[1]?mm:"0"+mmChars[0]) + '-' + (ddChars[1]?dd:"0"+ddChars[0]);
			}
			if (startDate == endDate) {
				return;
			}else{
				$("#loader").show();
				$("#dashboard_data").hide();
				$.ajax({
						url: '/get_session',
						type: 'get',
						data: {
							startDate: startDate,
							endDate : endDate,
							dashboardClientId: dashboardClientId
						},
						success: function(response){
							//~ console.log(response)
							$('#dashboard_traffic').find('.traffic').html(response.sessions);
							$('#dashboard_traffic').find('.bounce_rate').html(financial(response.bounceRate));
							$('#dashboard_traffic').find('.users').html(response.newUsers);
							//~ traffic
						},
						error: function(er){
							console.log(er)
						}
				});
				//~ $('#exampleModal').find('.report_leads').html('<div class="m-2 text-center"><h3>Loading...</h3></div>');
				$.ajax({
						url: '/get_leads',
						type: 'get',
						data: {
							startDate: startDate,
							endDate : endDate,
							dashboardClientId: dashboardClientId
						},
						success: function(response){
							$("#loader").hide();
							$("#dashboard_data").show();
							var table_leads = $('#data_lead').dataTable({
								//~ "processing": true,
								"searching": false,
								"destroy": true,
								"bLengthChange" : false, //thought this line could hide the LengthMenu
								"bInfo":false,
								"pageLength": 8,
								//~ "fnDrawCallback": function(oSettings) {
									//~ if ($('#data_lead tr').length < 8) {
										//~ $('.dataTables_paginate').hide();
									//~ }
								//~ },
								"language": {
									"oPaginate": {
										"sNext": '<i class="fa fa-angle-right"></i>',
										"sPrevious": '<i class="fa fa-angle-left"></i>',
										"sFirst": '<i class="fa fa-step-backward"></i>',
										"sLast": '<i class="fa fa-step-forward"></i>' 
									}
									  
								} ,
								'columnDefs': [{
									 'targets': 0,
									 'searchable': false,
									 'orderable': false,
									 'width': '1%',
									 'className': 'dt-body-center',
									 'render': function (data, type, full, meta){
										 return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
										 
									}
								},
								{
									 'targets': 5,
									 'searchable': false,
									 'orderable': false,
									 'width': '1%',
									 'className': 'dt-body-center',
									 'render': function (data, type, full, meta){
										return '<a href="" class="editor_remove"><i class="ni ni-basket"></i></a>';
									}
								},
								{
									 'targets': 1,
									 'searchable': false,
									 'orderable': false,
									 'width': '1%',
									 'className': 'dt-body-center',
									 'render': function (data, type, full, meta){
										 return '<a href="' + data + '" data-toggle="modal" class="lead_id" id ="lead_name">' + data + '</a>';
									}
								},
								{
									 'targets': 4,
									 'searchable': false,
									 'orderable': false,
									 'className': 'dt-body-center dateprograms',
									  "render": function(data) {
											return data;
									}
								}
								
								],
								"order": [[4, 'desc']],
								//~ 'columnDefs': [],
								
								data: response,
							});
							var buttons = new $.fn.dataTable.Buttons(table_leads, {
							buttons: [
								{ 
									text: 'Export',
									extend: 'csv',
									'className': 'btn btn-primary btn-sm',
									init: function( api, node, config) {
									   $(node).removeClass('dt-button buttons-csv buttons-html5')
									}
								}
								//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
								]
							}).container().appendTo($('#buttons_leads'));
							
							$('#data_lead').on('click', 'a.editor_remove', function (e) {
								e.preventDefault();
								$(this).parent("td").parent("tr").hide();
								
							} );
						},
						error: function(er){
							console.log(er)
						}
				});
				
				$.ajax({
				url: '/get_querries',
				type: 'get',
				data: {
					startDate: startDate,
					endDate : endDate,
					dashboardClientId: dashboardClientId
				},
				success: function(response){
					$('#dashboard_traffic').find('.avg_position').html(financial(response["pages"][0][1]));
					var table_querry  = $('#data_querries').dataTable({
						//~ "processing": true,
						"searching": false,
						"destroy": true,
						"bLengthChange" : false, //thought this line could hide the LengthMenu
						"bInfo":false,
						"pageLength": 9,
						"language": {
							"oPaginate": {
								"sNext": '<i class="fa fa-angle-right"></i>',
								"sPrevious": '<i class="fa fa-angle-left"></i>',
								"sFirst": '<i class="fa fa-step-backward"></i>',
								"sLast": '<i class="fa fa-step-forward"></i>' 
							}
							  
						} ,
						columnDefs: [
							{ 
								targets: 1,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							},
							{ 
								targets: 3,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							}
						],
						//~ "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
						   //~ var index = iDisplayIndex +1;
						   //~ $('td:eq(0)',nRow).html(index);
						   //~ return nRow;
						//~ },
						data: response['querries'],
					 });
						var buttons = new $.fn.dataTable.Buttons(table_querry, {
							buttons: [
								{ 
								text: 'Export',
								extend: 'csv',
								'className': 'btn btn-primary btn-sm',
								init: function( api, node, config) {
								   $(node).removeClass('dt-button buttons-csv buttons-html5')
								}
								}
							//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
							]
						}).container().appendTo($('#buttons_querry'));
					 
					var table_websites = $('#data_website').dataTable({
						"processing": true,
						"searching": false,
						"destroy": true,
						"bLengthChange" : false, //thought this line could hide the LengthMenu
						"bInfo":false,
						"pageLength": 9,
						"language": {
							"oPaginate": {
								"sNext": '<i class="fa fa-angle-right"></i>',
								"sPrevious": '<i class="fa fa-angle-left"></i>',
								"sFirst": '<i class="fa fa-step-backward"></i>',
								"sLast": '<i class="fa fa-step-forward"></i>' 
							}
							  
						} ,
						columnDefs: [
							{ 
								targets: 1,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							},
							{ 
								targets: 3,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							}
						],
						
						data: response['pages'],
					 });
					 var buttons = new $.fn.dataTable.Buttons(table_websites, {
						buttons: [
							{ 
							text: 'Export',
							extend: 'csv',
							'className': 'btn btn-primary btn-sm',
							init: function( api, node, config) {
							   $(node).removeClass('dt-button buttons-csv buttons-html5')
							}
							}
						//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
						]
					}).container().appendTo($('#buttons_websites'));
					var table_devices = $('#data_countries').dataTable({
						"processing": true,
						"searching": false,
						"bLengthChange" : false, //thought this line could hide the LengthMenu
						"bInfo":false,
						"destroy": true,
						"pageLength":9,
						"language": {
							"oPaginate": {
								"sNext": '<i class="fa fa-angle-right"></i>',
								"sPrevious": '<i class="fa fa-angle-left"></i>',
								"sFirst": '<i class="fa fa-step-backward"></i>',
								"sLast": '<i class="fa fa-step-forward"></i>' 
							}
							  
						} ,
						columnDefs: [
							{ 
								targets: 1,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							},
							{ 
								targets: 3,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							}
						],
						data: response['countries'],
					 });
					var buttons = new $.fn.dataTable.Buttons(table_querry, {
						buttons: [
							{ 
							text: 'Export',
							extend: 'csv',
							'className': 'btn btn-primary btn-sm',
							init: function( api, node, config) {
							   $(node).removeClass('dt-button buttons-csv buttons-html5')
							}
							}
						//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
						]
					}).container().appendTo($('#buttons_devices'));
					
					var table_countries = $('#data_devices').dataTable({
						"processing": true,
						"searching": false,
						"bLengthChange" : false, //thought this line could hide the LengthMenu
						"bInfo":false,
						"destroy": true,
						"pageLength": 9,
						"language": {
							"oPaginate": {
								"sNext": '<i class="fa fa-angle-right"></i>',
								"sPrevious": '<i class="fa fa-angle-left"></i>',
								"sFirst": '<i class="fa fa-step-backward"></i>',
								"sLast": '<i class="fa fa-step-forward"></i>' 
							}
							  
						} ,
						columnDefs: [
							{ 
								targets: 1,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							},
							{ 
								targets: 3,
								render: function ( data, type, row ) {
									return (querryctr(data));
								}
							}
						],
						data: response['devices'],
					 });
					 var buttons = new $.fn.dataTable.Buttons(table_countries, {
						buttons: [
							{ 
							text: 'Export',
							extend: 'csv',
							'className': 'btn btn-primary btn-sm',
							init: function( api, node, config) {
							   $(node).removeClass('dt-button buttons-csv buttons-html5')
							}
							}
						//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
						]
					}).container().appendTo($('#buttons_countries'));
				//~ console.log(response['countries'])
					//~ traffic
				},
				error: function(er){
					console.log(er)
				}
		});
				
				$.ajax({
						url: '/get_channels',
						type: 'get',
						data: {
							startDate: startDate,
							endDate : endDate,
							dashboardClientId: dashboardClientId
						},
						success: function(response){
							var table_channels= $('#data_channel').dataTable({
								//~ "processing": true,
								"searching": false,
								"bLengthChange" : false, //thought this line could hide the LengthMenu
								"bInfo":false,
								"bPaginate":false,
								"destroy": true,
								"pageLength": 4,
								data: response,
							});
							var buttons = new $.fn.dataTable.Buttons(table_channels, {
								buttons: [
									{ 
									text: 'Export',
									extend: 'csv',
									'className': 'btn btn-primary btn-sm',
									init: function( api, node, config) {
									   $(node).removeClass('dt-button buttons-csv buttons-html5')
									}
									}
								//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
								]
							}).container().appendTo($('#buttons_channels'));
						},
						error: function(er){
							console.log(er)
						}
				});
				$.ajax({
						url: '/get_sources',
						type: 'get',
						data: {
							startDate: startDate,
							endDate : endDate,
							dashboardClientId: dashboardClientId
						},
						success: function(response){
							var table_sources = $('#data_scope').dataTable({
							//~ "processing": true,
							//editor_removeconsole.log(page.len())
							"searching": false,
							"bLengthChange" : false, //thought this line could hide the LengthMenu
							"bInfo":false,
							"destroy": true,
							"pageLength": 5,
							"language": {
								"oPaginate": {
									"sNext": '<i class="fa fa-angle-right"></i>',
									"sPrevious": '<i class="fa fa-angle-left"></i>',
									"sFirst": '<i class="fa fa-step-backward"></i>',
									"sLast": '<i class="fa fa-step-forward"></i>' 
								}
								  
							} ,
							data: response,
							});
							var buttons = new $.fn.dataTable.Buttons(table_sources, {
								buttons: [
									{ 
									text: 'Export',
									extend: 'csv',
									'className': 'btn btn-primary btn-sm',
									init: function( api, node, config) {
									   $(node).removeClass('dt-button buttons-csv buttons-html5')
									}
									}
								//~ <button class="btn btn-primary btn-sm" id="channel_export">Export</button>
								]
							}).container().appendTo($('#buttons_source'));
						},
						error: function(er){
							console.log(er)
						}
				});
				    //~ $(document).on("click",'.company_id',function(){
					//~ var company_id = $(this).attr('href');
					//~ console.log(company_id);
					//~ )};
					   
				   $(document).on("click",'.lead_id',function(){
				   $('#exampleModal').find('.report_leads').html('<div class="m-2 text-center"><h3>Loading...</h3></div>');
				   $('#exampleModal').modal('show');
				   var status_id = $(this).attr('href');
					$.ajax({
						url: '/get_leads_details',
						type: 'get',
						data: {
							id: status_id,
							dashboardClientId: dashboardClientId,
							startDate: startDate,
							endDate : endDate
						},
						success: function(response){
						$('#exampleModal').find('.report_leads').html(response);
						$('#exampleModal').find('#currentLeadName').val(status_id);
						$('#exampleModal').find('.lead_title').html(status_id);
						
						//~ $('#exampleModal').find("#linkedin_script").html('<script type="IN/CompanyInsider" data-id="' + $("#currentLeadName").val() + '" data-format="inline"><\/script>')
						},
						error: function(er){
							console.log(er)
						}
					});
					//~ var modal_data = $('#exampleModal').find('.report_leads')
					//~ modal_data.loads
					//~ console.log(here);
					//~ $('#exampleModal').modal('show');
				   //~ alert(status_id); 
				});
				function clearLeadRows(that,leadId){
					var nextRow=that.closest('tr').next();
					if(nextRow && nextRow.hasClass('lead_'+leadId)){
						nextRow.remove();
					}else{
						return;
					}
					clearLeadRows(that,leadId);
				}
				function createLeadRows(that, rows, row_id){
					clearLeadRows(that,row_id);
					rows.forEach(function(rowData){
						that.closest('tr').after('<tr class="lead_per_sessions lead_'+row_id+'"><td></td><td>'+rowData[0]+'</td><td>'+rowData[1]+'</td><td></td><td></td></tr>');
					});
				}
				
				//~ $('#check_all').click(function() {
					//~ $('input[name=leads]').prop('checked', true);
				//~ });
				$("#check_all").change(function(){
				 var checked = $(this).is(':checked');
				 if(checked){
				   $(".chk_all_leads").each(function(){
					 $(this).prop("checked",true);
				   });
				 }else{
				   $(".chk_all_leads").each(function(){
					 $(this).prop("checked",false);
				   });
				 }
				});
			 
			  // Changing state of CheckAll checkbox 
				//~ $(".checkbox").click(function(){
					//~ if($(".checkbox").length == $(".checkbox:checked").length) {
					  //~ $("#checkall").prop("checked", true);
					//~ } else {
					  //~ $("#checkall").removeAttr("checked");
					//~ }

				//~ });
				//~ $('#check_all').click(function() {
					//~ $('input[name=leads]').prop('checked', false);
				//~ });
				
				$(document).on('click','.lead-record',function(e){
					var that=$(this);
					var lead_name = $('#exampleModal').find('#currentLeadName').val();
					var lead_date = that.data('leaddate');
					var row_id=lead_name.toLowerCase().replace(/ /g,'-').replace(/[^\w-]+/g,'') + lead_date;
					clearLeadRows(that, row_id);
					that.closest('tr').after('<tr class="lead_per_sessions lead_'+row_id+'"><td colspan="5">Loading...</td></tr>')
					$.ajax({
						url: '/get_leads_data',
						type: 'get',
						data: {
							lead_name: lead_name,
							lead_date : lead_date,
							dashboardClientId: dashboardClientId,
							startDate: startDate,
							endDate : endDate
						},
						success: function(response){
							createLeadRows(that,response,row_id);
						},
						error: function(er){
							console.log(er)
						}
					});
				})
			}
		});
        
		
    });
